#include "Peg.h"

Peg::Peg(Color var) {
	color = var;
}

char Peg::getColorName(){
	return ColorNames[color];
}

Color Peg::getColor(){
	return color;
}
