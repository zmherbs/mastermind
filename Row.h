#ifndef __ROW_H__
#define __ROW_H__

#include "Peg.h"

#include <vector>

const int ROW_SIZE = 4;

class Row{
	private:
		std::vector<Peg> placed_pegs = std::vector<Peg> (ROW_SIZE, Peg(EMPTY));
		std::vector<Peg> correct_check_pegs = std::vector<Peg> (ROW_SIZE, Peg(EMPTY));

	public:
		Peg getPeg(int peg);
		Peg getCheckPeg(int peg);

		void setPeg(int num, Peg peg);
		void setCheckPeg(int num, Peg peg);

		void printRow();
};

#endif
