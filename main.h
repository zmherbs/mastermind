#ifndef __MAIN_H__
#define __MAIN_H__

#include "Board.h"

//might make sense to consolidate these into a single pair set, idk though
std::map <Color, char> ColorNames = {
	{EMPTY, 'E'},
	{RED, 'R'},
	{TEAL, 'T'},
	{GREEN, 'G'},
	{YELLOW, 'Y'},
	{PURPLE, 'P'},
	{ORANGE, 'O'},
	{BLACK, 'B'},
	{WHITE, 'W'}
};

std::map <char, Color> NamesColor = {
	{'E', EMPTY},
	{'R', RED},
	{'T', TEAL},
	{'G', GREEN},
	{'Y', YELLOW},
	{'P', PURPLE},
	{'O', ORANGE},
	{'B', BLACK},
	{'W', WHITE}
};

#endif
