#include "Row.h"

#include <iostream>

using namespace std;

Peg Row::getPeg(int peg){
	return placed_pegs[peg];
}

Peg Row::getCheckPeg(int peg){
	return correct_check_pegs[peg];
}

void Row::setPeg(int num, Peg peg){
	placed_pegs[num] = peg;
}

void Row::setCheckPeg(int num, Peg peg){
	correct_check_pegs[num] = peg;
}

void Row::printRow(){
	for (int i = 0; i < ROW_SIZE; i++){
		cout << getPeg(i).getColorName() << '\t';
	}

	cout << "|\t";

	for (int i = 0; i < ROW_SIZE; i++){
		cout << getCheckPeg(i).getColorName() << '\t';
	}

	cout << endl;
}
