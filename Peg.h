#ifndef __PEG_H__
#define __PEG_H_

#include <map>

const int NUM_COLORS = 6;//changes whenever more or less colors are added

enum Color {
	EMPTY,
	RED,
	TEAL,
	GREEN,
	YELLOW,
	PURPLE,
	ORANGE,
	BLACK,
	WHITE
};


extern std::map <Color, char> ColorNames;
extern std::map <char, Color> NamesColor;

class Peg{
	private:
		Color color;

	public:
		Peg(Color var);

		Color getColor();
		char getColorName();
};

#endif
