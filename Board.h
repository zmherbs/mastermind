#ifndef __BOARD_H__
#define __BOARD_H_

#include "Row.h"

#include <vector>

const int NUM_TRIES = 8;

class Board {
	private:
		std::vector<Row> player_rows = std::vector<Row>(NUM_TRIES);
		Row master_row;

		void setMaster(const std::string& preset = "");
		bool compareRowToMaster();
		void printGuessesSoFar();

	public:
		Board();
		Board(std::string preset);
		bool setRow(int round, std::string guess);
		void finishGame(bool won);
};

#endif
