#include "main.h"

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <vector>
#include <string>
#include <unistd.h>

using namespace std;

int round = 0;//multiple places need to know round number, so idk if its best to make it global or not but doing so anyway

int main(int argc, char *argv[]){
	Board board;

	int opt;
	while ((opt = getopt(argc, argv, "f")) != -1){
		switch (opt) {
			case 'f':{
				//getting Board from input
				string master;
				cin >> master;
				board = Board(master);
				break;
			}
			default:{
				break;
			}
		}
	}

	cout << "Hello! Lets play a game. I've made a code of 4 pegs that could each be one of the following of the colors:\n";
	cout << "R, T, G, Y, P, O\n";
	cout << "Can you guess which pegs are which colors? You have " << NUM_TRIES << " tries.\n";
	cout << "After each guess I'll tell you how you did.\n";
	cout << "Each B means that you got a color and position right.\n";
	cout << "Each W means that a color is right, but not the position.\n";
	cout << "Let's start!\n\n";

	do{
		cout << "Please put your guess with 4 letters as a single word (no spaces): ";

		string guess;
		cin >> guess;
		cout << endl;
		if (!board.setRow(round, guess)){
			round++;
		}
		else{
			board.finishGame(true);
			return 0;
		}
	}while(round < NUM_TRIES);
	board.finishGame(false);
	return 0;
}
