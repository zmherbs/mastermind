#include "Board.h"

#include <string>
#include <iostream>

using namespace std;

extern int round;

Board::Board(){
	setMaster();
}

Board::Board(string preset){
	setMaster(preset);
}

bool Board::compareRowToMaster(){
	int num_black = 0, num_white = 0;

	Row master_row_copy = master_row;
	Row this_row_copy = player_rows[round];

	//scan rows for blacks, the rescan for whites
	for (int i = 0; i < ROW_SIZE; i++){
		if (this_row_copy.getPeg(i).getColor() == master_row_copy.getPeg(i).getColor()){
			num_black++;
			master_row_copy.setPeg(i, Peg(EMPTY));
			this_row_copy.setPeg(i, Peg(EMPTY));
		}
	}

	for (int i = 0; i < ROW_SIZE; i++){
		if(this_row_copy.getPeg(i).getColor() != EMPTY){
			for (int j = 0; j < ROW_SIZE; j++){
				if (this_row_copy.getPeg(i).getColor() == master_row_copy.getPeg(j).getColor()){
					num_white++;
					master_row_copy.setPeg(j, Peg(EMPTY));
					j = ROW_SIZE;
				}
			}
		}
	}

	bool is_correct = false;
	if (num_black == 4){
		is_correct = true;
	}

	int correct_peg_num = 0;
	while (num_black > 0){
		player_rows[round].setCheckPeg(correct_peg_num, Peg(BLACK));
		correct_peg_num++;
		num_black--;
	}

	while (num_white > 0){
		player_rows[round].setCheckPeg(correct_peg_num, Peg(WHITE));
		correct_peg_num++;
		num_white--;
	}

	return is_correct;
}

void Board::setMaster(const string& preset){
	srand(time(NULL));

	if (preset.compare("") == 0){
		for (int i = 0; i < ROW_SIZE; i++){
			int value = rand() % NUM_COLORS + 1;
			Color color = static_cast<Color>(value);
			master_row.setPeg(i, Peg(color));
		}
	}
	else{
		for (int i = 0; i < ROW_SIZE; i++){
			if (preset[i] == 'B' || preset[i] == 'W'){
				cout << "Invalid master row. Please restart with a valid row.\n";
				exit(1);
			}
			Color color = NamesColor.at(preset[i]);
			master_row.setPeg(i, Peg(color));
		}
	}

	//master_row.printRow();
}

bool Board::setRow(int round, string guess){
	//returns if row matches master
	//assuming properly formatted and legal input (I know this is bad, but just trying to get to work)
	for (int i = 0; i < ROW_SIZE; i++){
		player_rows.at(round).setPeg(i, Peg(NamesColor[guess[i]]));
	}
	bool winner = compareRowToMaster();
	printGuessesSoFar();

	return winner;
}

void Board::printGuessesSoFar(){
	for (int i = 0; i <= round; i++){
		player_rows.at(i).printRow();
	}
}

void Board::finishGame(bool won){
	if (won){
		cout << "You guessed the master row correctly! You won!\n";
	}
	else{
		cout << "Thanks for trying! The master row was:\n";
		master_row.printRow();
		cout << "You'll get it next time!\n";
	}
	cout << "\nPlease play again!\n";
}
